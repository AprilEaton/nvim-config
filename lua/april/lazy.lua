local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

local plugins = {
    {
        'nvim-telescope/telescope.nvim',
        dependencies = { { 'nvim-lua/plenary.nvim' } },
        name = "Telescope",
    },

    {
        'nvim-treesitter/nvim-treesitter',
        build = ":TSUpdate",
        name = "Treesitter",
    },

    { 'nvim-treesitter/playground',              name = "Treesitter Playground" },
    { 'ThePrimeagen/harpoon',                    name = "Harpoon" },
    { 'theprimeagen/refactoring.nvim',           name = "Refactoring" },
    { 'mbbill/undotree',                         name = "Undotree" },
    { 'nvim-treesitter/nvim-treesitter-context', name = "Treesitter Context" },

    {
        'VonHeikemen/lsp-zero.nvim',
        branch = 'v1.x',
        dependencies = {
            -- LSP Support
            { 'neovim/nvim-lspconfig',             name = "LSP Config" },
            { 'williamboman/mason.nvim',           name = "Mason" },
            { 'williamboman/mason-lspconfig.nvim', name = "Mason Config" },

            -- Autocompletion
            { 'hrsh7th/nvim-cmp',                  name = "Nvim Complete" },
            { 'hrsh7th/cmp-buffer',                name = "Complete Buffer" },
            { 'hrsh7th/cmp-path',                  name = "Complete Path" },
            { 'saadparwaiz1/cmp_luasnip',          name = "Complete LuaSnip" },
            { 'hrsh7th/cmp-nvim-lsp',              name = "Complete LSP" },
            { 'hrsh7th/cmp-nvim-lua',              name = "Complete Lua" },

            -- Snippets
            { 'L3MON4D3/LuaSnip',                  name = "LuaSnip" },
            { 'rafamadriz/friendly-snippets',      name = "Friendly Snippets" },
        },
        name = "LSP Zero",
    },

    {
        'weilbith/nvim-code-action-menu',
        build = 'CodeActionMenu',
        name = "Code Action Menu",
    },

    {
        'akinsho/flutter-tools.nvim',
        dependencies = {
            'nvim-lua/plenary.nvim',
            'stevearc/dressing.nvim',
        },
        name = "Flutter Tools",
        lazy = true,
    },

    {
        "folke/which-key.nvim",
        config = function()
            vim.o.timeout = true
            vim.o.timeoutlen = 300
        end,
        name = "Which Key",
    },

    {
        'iamcco/markdown-preview.nvim',
        build = 'cd app && npm install',
        config = function()
            vim.g.mkdp_filetypes = { 'markdown' }
        end,
        name = "Markdown Preview",
        lazy = true,
    },

    { 'christoomey/vim-tmux-navigator', name = "Vim-Tmux Navigator" },

    {
        'ecthelionvi/NeoComposer.nvim',
        dependencies = { 'kkharji/sqlite.lua' },
        name = "NeoComposer",
    },

    { 'AckslD/muren.nvim',   name = "Muren" },

    {
        'vimwiki/vimwiki',
        name = "VimWiki",
        init = function()
            vim.g.vimwiki_list = { {
                path = '~/vimwiki/',
                syntax = 'markdown',
                ext = '.md',
            } }

            vim.g.vimwiki_global_ext = 0
        end
    },

    {
        "oberblastmeister/neuron.nvim",
        dependencies = {
            { 'nvim-lua/popup.nvim',           name = "Popup" },
            { "nvim-lua/plenary.nvim",         name = "Plenary" },
            { "nvim-telescope/telescope.nvim", name = "Telescope" }
        }
    },

    { "catppuccin/nvim",     name = "catppuccin" },

    { 'tpope/vim-obsession', name = "Vim Obsession" },

    {
        'willothy/nvim-cokeline',
        dependencies = { 'kyazdani42/nvim-web-devicons', name = "Web Dev Icons" },
        name = "Cokeline",
    },

    {
        'axkirillov/hbac.nvim',
        name = "Heuristic buffer auto-close"
    },

    { 'rcarriga/nvim-notify', name = 'Notify' },

    {
        'folke/noice.nvim',
        dependencies = {
            { 'MunifTanjim/nui.nvim', name = 'Neovim UI' },
            { 'rcarriga/nvim-notify', name = 'Notify' }
        },
        name = 'Noice',
    },

    {
        "nvim-neorg/neorg",
        build = ":Neorg sync-parsers",
        dependencies = { {
            "nvim-lua/plenary.nvim",
            { "nvim-neorg/neorg-telescope", name = "Neorg-Telescope", }
        } },
        name = "Neorg",
    },

    -- { 'kelly-lin/ranger.nvim',          name = "Ranger" },
    {
        'stevearc/oil.nvim',
        dependencies = { "nvim-tree/nvim-web-devicons" },
        name = "Oil"
    }
}

local lazyconf = {}

require("lazy").setup(plugins, lazyconf)
