vim.notify = require('notify')
local notif = require('notify')

notif.setup({
    fps = 30,
    level = 4,
    timeout = 10,
    max_width = 15,
    max_height = 5,
    render = 'default',
    top_down = true,
    stages = 'slide',
    icons = {
        DEBUG = '󰠭',
        ERROR = '',
        INFO = '󰋼',
        TRACE = '',
        WARN = '',
    }
})
