require("catppuccin").setup({
    flavour = "macchiato", -- latte, frappe, macchiato, mocha
    background = {
        -- :h background
        light = "latte",
        dark = "macchiato",
    },
    transparent_background = false,
    show_end_of_buffer = true, -- show the '~' characters after the end of buffers
    term_colors = true,
    dim_inactive = {
        enabled = false,
        shade = "dark",
        percentage = 0.15,
    },
    no_italic = false, -- Force no italic
    no_bold = false,   -- Force no bold
    styles = {
        comments = { "italic" },
        conditionals = { "italic" },
        loops = { "italic" },
        functions = { "italic" },
        keywords = { "italic" },
        strings = {},
        variables = { "italic" },
        numbers = { "bold" },
        booleans = { "bold" },
        properties = { "italic" },
        types = { "italic" },
        operators = {},
    },
    color_overrides = {},
    custom_highlights = function(colors)
        return {
            WhichKeyBorder = { fg = colors.mauve },
            WhichKey = { fg = colors.pink },
            WhichKeyGroup = { fg = colors.mauve },
            WhichKeySeparator = { fg = colors.lavender },
            WhichKeyDesc = { fg = colors.peach },
            WhichKeyValue = { fg = colors.overlay0 },
        }
    end,
    integrations = {
        cmp = true,
        gitsigns = true,
        nvimtree = true,
        telescope = true,
        notify = false,
        mini = false,
        harpoon = true,
        which_key = true,
        -- For more plugins integrations please scroll down (https://github.com/catppuccin/nvim#integrations)
    },
})

-- setup must be called before loading
vim.cmd.colorscheme "catppuccin"
