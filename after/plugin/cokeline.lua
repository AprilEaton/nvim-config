require('cokeline').setup({
    show_if_buffers_are_at_least = 2, -- No need for a bufferline if there's only 1 buffer
    buffers = {
        focus_on_delete = 'prev',
        new_buffers_position = 'next',
        delete_on_right_click = false,
    },
    mappings = {
        cycle_prev_next = true,
    },
    rendering = {
        max_buffer_width = 25,
    }
})

-- Gonna go ahead and add all my buffer keybinds here so they're in the same place

local keyset = vim.keymap.set;

keyset('n', '<leader>bd', ':bd<CR>', { desc = 'Delete the current buffer' })
keyset('n', '<leader>bn', ':bn<CR>', { desc = 'Go to the next buffer' })
