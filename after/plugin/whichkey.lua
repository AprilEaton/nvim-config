local wk = require("which-key")

wk.setup {
    plugins = {
        marks = true,
        registers = true,
        spelling = { enabled = false },
        presets = {
            operators = false,
            motions = false,
            text_objects = true,
            windows = true,
            nav = true,
            z = true,
            g = true,
        },
    },

    key_lables = {
        ["<space>"] = "SPC",
        ["<CR>"] = "ENT",
        ["<tab>"] = "TAB",
    },

    icons = {
        breadcrumb = "»",
        separator = "➜",
        group = "+",
    },

    popup_mappings = {
        scroll_down = "<C-d>",
        scroll_up = "<C-f>",
    },

    window = {
        border = "double",
        position = "top",
        margin = { 1, 1, 1, 1 },
        padding = { 1, 2, 1, 2 },
        winblend = 10,
        zindex = 1000,
    },

    layout = {
        height = { min = 4, max = 25 },
        width = { min = 20, max = 50 },
        spacing = 4,
        align = "left",
    },

    ignore_missing = false,

    hidden = { "<silent>", "<cmd>", "<Cmd>", "<CR>", "^:", "^ ", "^call ", "^lua " },

    show_help = true,

    show_keys = true,

    triggers = "auto",

}

wk.register({
    a = { "Action" },
    f = { "Format buffer" },
    s = { "Find and replace" },
    u = { "Undo tree" },
    x = { "Make executable" },
    y = { "Yank to system clipboard" },
    Y = { "Yank lines to system clipboard" },
    d = {
        name = "Diagnosis",
        d = { "Document" },
        w = { "Workspace" },
    },
    p = {
        name = "Find",
        f = { "Fuzzy find" },
        s = { "Grep" },
        v = { "Explore" },
    },
    r = {
        name = "Refactor",
        b = { "Extract" },
        i = { "Inline" },
    },
}, { prefix = "<leader>" })
