local hbac = require('hbac')

hbac.setup({
    autoclose = true,
    threshold = 15,
    close_command = function(bufnr)
        vim.api.nvim_buf_delete(bufnr, {})
    end,
    close_buffers_with_windows = true,
})

vim.keymap.set('n', '<leader>bp', hbac.toggle_pin, { desc = 'Pin the current buffer'})
vim.keymap.set('n', '<leader>bc', hbac.close_unpinned, { desc = 'Close all unpinned buffers' })
